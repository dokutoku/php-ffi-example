# FFI-example

Example for using FFI in PHP.

## Usage

After building all the dynamic libraries for each language in the langs directory, enter the following command.

```
php test.php
```

## Check items

- The FFI module must be loaded
- The value of ffi.enable. Must be set to "1" if the PHP execution environment is not a console.
- PHP architecture and library architecture must match
- When used in a website, it is necessary to make sure that the libraries and their sources are not accessible from outside.

## Web server startup command

The default value of ffi.enable is zero, so if you want to start a PHP build-in web server, you have to override the setting by doing the following

```
php -S localhost:8080 -d ffi.enable=1
```
