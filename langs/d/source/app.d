/**
 * A dynamic library for use with PHP FFI.
 *
 * License: CC0
 */
module ffi_test.app;


private static import core.stdc.stdint;
private static import core.stdc.stdio;
private static import core.sys.windows.basetsd;
private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;

alias callback_t = extern (C) pure nothrow @nogc @live int function(int);

extern (C)
struct test
{
	int int_value;
	double float_value;
	const (char)* string_value;
	.callback_t callback_value;
}

extern (C)
pure nothrow @safe @nogc @live
export immutable (char)* hello_ffi()

	do
	{
		return "Hello, FFI World!";
	}

extern (C)
pure nothrow @safe @nogc @live
export int ffi_test()

	do
	{
		return 0;
	}

extern (C)
nothrow @nogc @live
export int printfln_int(core.stdc.stdint.int32_t input)

	do
	{
		return core.stdc.stdio.printf("%d\n", input);
	}

extern (C)
nothrow @nogc @live
export int printfln_string(const (char)* input)

	do
	{
		return core.stdc.stdio.printf("%s\n", input);
	}

extern (C)
nothrow @nogc @live
export int printfln_float(double input)

	do
	{
		return core.stdc.stdio.printf("%e\n", input);
	}

extern (C)
nothrow @nogc @live
export int printf_string(const (char)* str)

	do
	{
		return core.stdc.stdio.printf(str);
	}

extern (C)
pure nothrow @nogc @live
export int custom_callback(callback_t callback)

	do
	{
		return callback(10);
	}

version (Windows) {
	extern (Windows)
	pure nothrow @safe @nogc @live
	export core.sys.windows.windef.BOOL DllMain(core.sys.windows.basetsd.HANDLE hModule, core.sys.windows.windef.DWORD reasonForCall, core.sys.windows.winnt.LPVOID lpReserved)

		do
		{
			return core.sys.windows.windef.TRUE;
		}
}
