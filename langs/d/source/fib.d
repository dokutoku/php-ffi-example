/**
 * Fibonacci functions
 *
 * See_Also: https://github.com/Random-People/Fibonacci/blob/master/fib.d
 * License: CC0
 */
module ffi_test.fib;


private static import core.stdc.stdio;

extern (C)
nothrow @nogc @live
export void fib_test(int max)

	do
	{
		core.stdc.stdio.printf("0\n");
		core.stdc.stdio.printf("1\n");
		int a = 1;
		int b = a + a;
		core.stdc.stdio.printf("%d\n", a);
		core.stdc.stdio.printf("%d\n", b);

		while (a < max) {
			a = a + b;
			b = a + b;
			core.stdc.stdio.printf("%d\n", a);
			core.stdc.stdio.printf("%d\n", b);
		}
	}

extern (C)
pure nothrow @safe @nogc @live
export void fib_test_noprint(int max)

	do
	{
		int a = 1;
		int b = a + a;

		while (a < max) {
			a = a + b;
			b = a + b;
		}
	}
