# D Language
This is an example of a dynamic library for use with PHP FFI.

## How to build

### Compiler installation
Download and install one of the following compilers.

- [DMD](https://dlang.org/download.html)
- [LDC](https://github.com/ldc-developers/ldc/releases/)

### Build command example

#### 32bit(Windows)

```
dub build --build=release-nobounds --arch=x86_mscoff --compiler=dmd
```

#### 32bit(linux)

```
dub build --build=release-nobounds --arch=x86 --compiler=dmd
```

#### 64bit

```
dub build --build=release-nobounds --arch=x86_64 --compiler=dmd
```
