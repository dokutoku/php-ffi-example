<?php
declare(strict_types=1);

/**
 * Fib benchmark.
 *
 * @license https://creativecommons.org/publicdomain/zero/1.0/deed CC0
 */
namespace FFI_Exsample\benchmark;


error_reporting(E_ALL);

if (!is_readable( __DIR__.'/lib/common.php')) {
	http_response_code(500);
	echo 'lib/common.php is not readable.', "\n";

	exit(1);
} else if (!is_readable( __DIR__.'/lib/fib.php')) {
	http_response_code(500);
	echo 'lib/fib.php is not readable.', "\n";

	exit(1);
} else if (!isset($argv)) {
	http_response_code(500);
	echo 'It can only be run in the console.', "\n";

	exit(1);
}

require __DIR__.'/lib/common.php';
require __DIR__.'/lib/fib.php';

const fib_count = 701408733;

function php_fib_time(int $count) : float
{
	$start_time = microtime(true);
	\FFI_Exsample\fib\fib_test($count);

	return microtime(true) - $start_time;
}

function php_fib_noprint_time(int $count) : float
{
	$start_time = microtime(true);
	\FFI_Exsample\fib\fib_test_noprint($count);

	return microtime(true) - $start_time;
}

function fib_time(\FFI $ffi, int $count) : float
{
	$start_time = microtime(true);
	$ffi->fib_test($count);

	return microtime(true) - $start_time;
}

function fib_noprint_time(\FFI $ffi, int $count) : float
{
	$start_time = microtime(true);
	$ffi->fib_test_noprint($count);

	return microtime(true) - $start_time;
}

function do_fib_bench(int $count) : void
{
	if ($count > 701408733) {
		throw new \Exception('The value is too large.');
	}

	$fib_cdef = file_get_contents(__DIR__.'/include/fib.h');
	echo 'Bench count: ', $count, "\n";
	$result_time = 0.0;

	//Other lang
	foreach (\FFI_Exsample\common\langs as $lang) {
		$ffi = \FFI_Exsample\common\create_ffi($fib_cdef, $lang, __DIR__);
		echo 'lang: ', $lang, "\n";

		$result_time = (string)(fib_time($ffi, $count));
		echo "\t", 'fib_time: ', $result_time, "\n";

		$result_time = (string)(fib_noprint_time($ffi, $count));
		echo "\t", 'fib_noprint_time: ', $result_time, "\n";
	}

	//PHP
	echo 'lang: PHP', "\n";

	$result_time = (string)(php_fib_time($count));
	echo "\t", 'fib_time: ', $result_time, "\n";

	$result_time = (string)(php_fib_noprint_time($count));
	echo "\t", 'fib_noprint_time: ', $result_time, "\n";
}

echo do_fib_bench(fib_count);
