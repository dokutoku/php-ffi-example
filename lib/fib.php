<?php
declare(strict_types=1);

namespace FFI_Exsample\fib;

/**
 * FIB functions
 *
 * @see https://github.com/Random-People/Fibonacci/blob/master/fib.php
 * @license https://creativecommons.org/publicdomain/zero/1.0/deed CC0
 */

/**
 * Do FIB
 */
function fib_test(int $max) : void
{
	$a = 1;
	$b = $a + $a;
	echo '0'."\n";
	echo '1'."\n";
	echo $a."\n";
	echo $b."\n";

	while ($a < $max){
		$a = $a + $b;
		$b = $a + $b;
		echo $a."\n";
		echo $b."\n";
	}
}

/**
 * Do FIB
 */
function fib_test_noprint(int $max) : void
{
	$a = 1;
	$b = $a + $a;

	while ($a < $max){
		$a = $a + $b;
		$b = $a + $b;
	}
}
