<?php
declare(strict_types=1);

/**
 * Common processing of projects in.
 *
 * @license https://creativecommons.org/publicdomain/zero/1.0/deed CC0
 */
namespace FFI_Exsample\common;

/**
 * A list of programming languages other than PHP.
 */
const langs =
[
	'd',
];

if (version_compare(PHP_VERSION, '7.4.0') < 0) {
	http_response_code(500);
	echo 'PHP 7.4 or higher is required.', "\n";

	exit(1);
}

if (!extension_loaded('ffi')) {
	http_response_code(500);
	echo 'The FFI extension is not loaded.', "\n";

	exit(1);
}

if (isset($argv)) {
	if ((ini_get('ffi.enable') !== '1') && (ini_get('ffi.enable') !== 'preload')) {
		http_response_code(500);
		echo 'ffi.enable is neither "1" nor "preload".', "\n";

		exit(1);
	}
} else {
	if (ini_get('ffi.enable') !== '1') {
		http_response_code(500);
		echo 'If you want to use FFI outside of console, you have to set ffi.enable to "1".', "\n";

		exit(1);
	}
}

/**
 * Returns the identifier name of the architecture currently being used.
 *
 * @return string Architecture type.
 */
function architecture_type() : string
{
	return (PHP_INT_SIZE === 8) ? ('x64') : ((PHP_INT_SIZE === 4) ? ('x86') : ('unknown'));
}

/**
 * Creates a new FFI object.
 *
 * @param string $def C declarations.
 * @param string $lang_name Programming language name.
 * @param string $dir_path PATH to the directory containing the shared libraries.
 *
 * @throws \FFI\Exception If the PATH is not readable, or if an FFI error occurs.
 *
 * @return \FFI FFI object.
 */
function create_ffi(string $def, string $lang_name, string $dir_path = __DIR__) : \FFI
{
	$shlib_path = $dir_path.DIRECTORY_SEPARATOR.'langs'.DIRECTORY_SEPARATOR.$lang_name.DIRECTORY_SEPARATOR.((DIRECTORY_SEPARATOR) === '/' ? ('lib') : ('')).'ffi_'.$lang_name.'_'.architecture_type().'.'.PHP_SHLIB_SUFFIX;

	if (!is_readable($shlib_path)) {
		throw new \Exception('The path "'.$shlib_path.'" is not readable.');
	}

	return \FFI::cdef($def, $shlib_path);
}


/**
 * Creates a new struct.
 *
 * @param FFI $ffi FFI object.
 * @param string $struct_name struct name.
 *
 * @throws \Exception|\FFI\Exception If $struct_name is not a string type, or if memory allocation fails, or if an FFI error occurs.
 *
 * @return \FFI\CData new struct.
 */
function new_struct(\FFI $ffi, string $struct_name) : \FFI\CData
{
	if (!is_string($struct_name)) {
		throw new \Exception('The type of the input value is not string.');
	}

	$memory = $ffi->new('struct '.$struct_name);

	if ($memory === null) {
		throw new \Exception('Memory allocation error.');
	}

	return $memory;												
}

/**
 * Print the value of int using printf.
 *
 * @param FFI $ffi FFI object.
 * @param int $input input value. The value range from -2147483648 to 2147483647.
 *
 * @throws \Exception|\FFI\Exception If $input is not an int type, or if an FFI error occurs.
 *
 * @return int The return value of the C language printf.
 */
function printfln_int(\FFI $ffi, int $input) : int
{
	if (!is_int($input)) {
		throw new \Exception('The type of the input value is not int.');
	}

	if (($input < -2147483648) || ($input > 2147483647)) {
		throw new \Exception('Invalid value: '.((string) ($input)));
	}

	return $ffi->printfln_int($input);
}

/**
 * Print the value of string using printf.
 *
 * @param FFI $ffi FFI object.
 * @param string $input input value. The maximum length of the input string is 4095 bytes. The value of null is invalid.
 *
 * @throws \Exception|\FFI\Exception If $input is not an string type, or if an FFI error occurs.
 *
 * @return int The return value of the C language printf.
 */
function printf_string(\FFI $ffi, string $input) : int
{
	if (!is_string($input)) {
		throw new \Exception('The type of the input value is not string.');
	}

	if (strlen($input) > 4095) {
		throw new \Exception('An input string that exceeds 4095 bytes.');
	}

	return $ffi->printf_string($input);
}

/**
 * Print the value of float using printf.
 * This function may not work as expected on some platforms.
 *
 * @param FFI $ffi FFI object.
 * @param float $input input value. The range of values is the range of the C language double type. Non-numeric value is invalid.
 *
 * @throws \Exception|\FFI\Exception If $input float not an int type, or if an FFI error occurs.
 *
 * @return int The return value of the C language printf.
 */
function printfln_float(\FFI $ffi, float $input) : int
{
	if (!is_float($input)) {
		throw new \Exception('The type of the input value is not float.');
	}

	if (is_nan($input)) {
		throw new \Exception('Invalid value: NaN');
	}

	return $ffi->printfln_float($input);
}
