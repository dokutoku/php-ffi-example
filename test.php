<?php
declare(strict_types=1);

/**
 * FFI test.
 *
 * @license https://creativecommons.org/publicdomain/zero/1.0/deed CC0
 */
namespace FFI_Exsample\test;


function custom_callback_func(int $input) : int
{
	return $input * 10;
}

error_reporting(E_ALL);

if (!is_readable( __DIR__.'/lib/common.php')) {
	http_response_code(500);
	echo 'lib/common.php is not readable.', "\n";

	exit(1);
} else if (!is_readable( __DIR__.'/include/cddef.h')) {
	http_response_code(500);
	echo 'include/cddef.h is not readable.', "\n";

	exit(1);
}

require __DIR__.'/lib/common.php';

$error_count = 0;
$test_cdef = file_get_contents(__DIR__.'/include/cddef.h');
header('Content-Type: text/plain');

const int_value = -10;
const float_value = -10.0;
const string_value = "test\n";

echo 'int_value: ', int_value, "\n";
echo 'float_value: ', float_value, "\n";
echo 'string_value: ', string_value, "\n";

foreach (\FFI_Exsample\common\langs as $lang) {
	echo 'lang: ', $lang, "\n";

	try {
		$ffi = \FFI_Exsample\common\create_ffi($test_cdef, $lang, __DIR__);

		/* Hello */
		echo $ffi->hello_ffi(), "\n";

		if (isset($argv)) {
			/* Basic types */
			echo 'printfln_int: ';
			\FFI_Exsample\common\printfln_int($ffi, int_value);

			echo 'printfln_float: ';
			\FFI_Exsample\common\printfln_float($ffi, float_value);

			echo 'printf_string: ';
			\FFI_Exsample\common\printf_string($ffi, string_value);
		} else {
			echo 'Skip printfln_int', "\n";
			echo 'Skip printfln_float', "\n";
			echo 'Skip printf_string', "\n";
		}

		/* Callback */
		echo 'custom_callback: ', $ffi->custom_callback('\FFI_Exsample\test\custom_callback_func'), "\n";
		echo 'custom_callback: ', $ffi->custom_callback(fn($i) => $i * 10), "\n";

		/* Struct */
		$struct = \FFI_Exsample\common\new_struct($ffi, 'test_struct');
		$struct->int_value = int_value;
		$struct->float_value = float_value;
		echo '$struct->int_value: ', $struct->int_value, "\n";
		echo '$struct->float_value: ', $struct->float_value, "\n";
	} catch (\Exception $e) {
		$error_count++;
		echo $e, "\n";
	}

	echo "\n";
}

if ($error_count !== 0) {
	echo $error_count.' errors occurred.', "\n";

	exit(1);
}
