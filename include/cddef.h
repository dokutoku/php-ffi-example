#include <stdint.h>

extern void ffi_test(void);
extern const char* hello_ffi(void);
extern int printfln_int(int32_t);
extern int printfln_float(double);
extern int printf_string(const char*);
typedef int (*callback_t)(int);
extern int custom_callback(callback_t);

struct test_struct
{
	int int_value;
	double float_value;
	const char* string_value;
	callback_t callback_value;
};
